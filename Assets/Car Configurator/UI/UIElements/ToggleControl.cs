﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleControl : MonoBehaviour
{
    public Text Label;
    public string text { get { return Label.text; } set { Label.text = value; } }
}
