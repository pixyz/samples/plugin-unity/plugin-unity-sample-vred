﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Pixyz.ImportSDK;
using Pixyz.Commons.Extensions;

public class VariantsDisplay : MonoBehaviour {

    public RectTransform transformContent;
    public RectTransform materialContent;
    public SectionControl sectionControlPrefab;
    public ToggleControl toggleControlPrefab;


    private GameObject importedModel;
    private VariantsManager variantsManager;


    private class GenericKeyValue {

        public object key;
        public object value;

        public void setKeyValue<T, U>(T k, U v) {key = k; value = v; }
    }
    Dictionary<Toggle, GenericKeyValue> dict = new Dictionary<Toggle, GenericKeyValue>();

    void Start()
    {
        // Get the last imported model and init GUI with its variants
        importedModel = Importer.LatestModelImportedObject?.gameObject;
        variantsManager = importedModel?.GetComponentInChildren<VariantsManager>();
        if (variantsManager == null) { this.gameObject.SetActive(false); return; }

        // Init GUI
        CreateVariantsContent<VariantsManager.MaterialSwitch, Material>(materialContent, variantsManager.MaterialSwitchList);
        CreateVariantsContent<VariantsManager.TransformSwitch, TransformVariant>(transformContent, variantsManager.TransformSwitchList);
    }

    void CreateVariantsContent<T, U>(RectTransform variantsContent, List<T> switchList)
    {
        if (switchList.Count == 0) { variantsContent.transform.parent.parent.parent.gameObject.SetActive(false); return; }
        foreach (var switchObject in switchList) {

            var name = switchObject.GetFieldValue<string>("name");
            List<U> variants = switchObject.GetFieldValue<List<U>>("variants");
            if (variants.Count == 0) continue;

            var subContent = Instantiate(sectionControlPrefab, variantsContent.transform);
            subContent.text = name;
            var toggleGroup = subContent.gameObject.AddComponent<ToggleGroup>();

            foreach (var variant in variants)
            {
                if (variant == null) { continue; }
                string variantName = variant.GetPropertyValue<string>("name");
                var toggle = Instantiate(toggleControlPrefab, variantsContent.transform);
                toggle.text = variantName;
                var m_Toggle = toggle.GetComponent<Toggle>();
                m_Toggle.group = toggleGroup;
                m_Toggle.onValueChanged.AddListener(delegate { ToggleValueChanged(m_Toggle, variantsContent); });

                GenericKeyValue switchVariant = new GenericKeyValue();
                switchVariant.setKeyValue<T, U>(switchObject, variant);

                dict.Add(m_Toggle.GetComponent<Toggle>(), switchVariant);
            }
        }
    }
    
    void ToggleValueChanged(Toggle m_Toggle, RectTransform variantsContent)
    {
        if (m_Toggle.isOn) {
            GenericKeyValue pair = dict[m_Toggle];
            var switchObject = pair.key as VariantsManager.Switch;
            var variant = pair.value;
            switchObject.selectVariant(variant);
        }
    }
}
