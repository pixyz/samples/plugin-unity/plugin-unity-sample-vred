# Car Configurator Sample

Automotive sample for [Pixyz Plugin for Unity](https://www.pixyz-software.com/plugin/).

## 

![Car Confifurator](./images/CarConfig.png)
*Model not included*

This sample shows how to take advantage of .vpb Pixyz Plugin for Unity importer.

The menus are generated automatically. This demo will work with any .vpb file imported with Pixyz Plugin for Unity.

## Requirements

* Pixyz Plugin for Unity
* Unity 2019.3
* VRED Pro or VRED Design binded to a valid license

## How to use

### **1. Import**

1. Go to *Pixyz > Import Model* and select a .vpb file. We will use the automotive sample from Autodesk (C:/ProgramData/Autodesk/VREDPro-xx.x/Examples/Automotive_Genesis.vpb)
2. Please refer to the [documentation](https://www.pixyz-software.com/documentations/html/2020.1/plugin4unity/VREDImportGroup.html) for more information on the import settings. In this tutorial we'll prefer Pixyz mesh over VRED one. The process will take more time but the number of triangles will be significantly less important for the same visual quality. If importing the Autodesk genesis sample, you can use the material mapping table at Assets/mapping_table_genesis.csv. It will override existing materials with the ones from the Measured Materials Library. Choose a Maximum mesh quality and select "Use Materials in Resources".

3. Check the root of the imported model, a Variants Manager component and a Variant Sets component should be placed there. They should match the switches and variant sets from your .vpb file
    
### **2. Stage**

1. Place the imported model under the rotating "platform" GameObject

2. Expand the imported animation .fbx in Assets/3DModels. Select the AnimStack element and duplicate it (CTRL+D). Select the duplicated animstack and open the Animation window (CTRL+6). Delete the animations you do not need. For the Genesis, we'll only keep the Doors animations. Drag and drop the anim stack on the GameObject containing the Animator component

### **3. Run**

The model rotates in the center of the scene and the doors are opening. Transform switches are listed in the left menu, material switches in the right one. Select the configuration you wish and enjoy!

